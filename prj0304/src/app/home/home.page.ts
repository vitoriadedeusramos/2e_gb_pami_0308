import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  titulo = "Vitoria Motors";
  cards = [

    {
      titulo: "Kawasaki Ninja 400",
      subtitulo: "",
      conteudo:"A Ninja 400 oferece a maior cilindrada da categoría, 399 cc, com a sofisticação da potência do motor bicilíndrico.",
      foto:"https://i.pinimg.com/originals/d2/4c/32/d24c328b31d653ca62b65c21bab0b37c.jpg"
    },

    {
      titulo: "BMW S 1000 RR",
      subtitulo: "",
      conteudo: "A superesportiva é o primeiro modelo de duas rodas da companhia alemã a trazer a chancela da divisão Motorsport (M), até então destinada apenas aos carros da companhia.",
      foto:"https://fotos.jornaldocarro.estadao.com.br/wp-content/uploads/2020/08/31161230/P90329147_highRes_bmw-motorrad-m-perfo-1160x831.jpg"
    },

    {
      titulo:"Kawasaki Z",
      subtitulo: "",
      conteudo: "O impressionante estilo Sugomi, pilotagem fácil e potência que invocam o espírito Kawasaki Z. Luzes de LED e um display TFT que oferecem a tecnologia mais recente. A Z650 irá energizar seus sentidos e inspirar todas as suas viagens.",
      foto:"https://mototour.com.br/fotos/noticias/5-motos-apresentadas-no-salao-de-toquio-que-devem-chegar-em-2020.jpg"
    },

    {
      titulo:"Twister CB",
      subtitulo: "",
      conteudo: "Com 6 marchas, o câmbio da Honda CB Twister possui engates precisos que geram menos esforço do motor e garantem economia de combustível, além de um excelente desempenho na pilotagem.",
      foto:"https://www.cirnemotos.com.br/image/renderImage?path=%2Fdados%2Fsistemas%2Fp2e%2FcirneMotos%2Fmotos%2F1895%2Ftwister_cbs_branco.jpg"
    }

  ];
  constructor() {}

}
